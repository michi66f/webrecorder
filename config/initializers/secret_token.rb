# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
WebRecorder::Application.config.secret_key_base = 'ef3b897997fc6f9b77fcfddb827cf1662517335de59adffa15172816678ebca9c0ab4538057c6e130232b98ac25a53887049583140afa7e5d74e3e47696e6fcf'
