module ApplicationHelper
    def page_title
        title = "WebRecorder"
        title = @page_title + " - " + title if @page_title
        title
    end
    
    def menu_link_to(text, path)
        link_to_unless_current(text, path) { content_tag(:span, text) }
    end
    
    def team_image_tag(team, options = {})
        if team.image.present?
            path = team_path(team, format: team.image.extension)
            link_to(image_tag(path, { alt: team.name }.merge(options)), path)
        else
            ""
        end
    end
end
