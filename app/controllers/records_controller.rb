# cording: utf-8

class RecordsController < ApplicationController

  def index
    @records = Record.all
  end
    
  def show
    @record = Record.find(params[:id])
  end
    
  def new
    @record = Record.new
    @matches = Match.all
  end
    
 def edit
    @record = Record.find(params[:id])
    @matches = Match.all
 end
    
 def create
    permitted_params = params.require(:record).permit(:match_id, :team, :event, :half, :recordtime)
    @record = Record.create(permitted_params)
    if (@record.match && @record.save)
      redirect_to @record.match, notice: "New record."
    else
      render "new"
    end
  end
    
  def update
    @record = Record.find(params[:id])
    permitted_params = params.require(:record).permit(:match_id, :team, :event, :half, :recordtime)
    @record.update_attributes(permitted_params)
    if (@record.match && @record.save)
      redirect_to @record.match, notice: "Record is updated."
    else
      render "edit"
    end
  end
    
  def destroy
    @record = Record.find(params[:id])
    @record.destroy
    redirect_to :records, notice: "Record deleted!"
  end
end
