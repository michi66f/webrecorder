# cording: utf-8

class MatchesController < ApplicationController

  def index
    @matches = Match.all
  end
    
  def show
    @match = Match.find(params[:id])
    #@teams = @match.teams.all
    #@records = @match.records.all
    #@records1st = @match.records.where(half: 1).order(:recordtime)
    #@records2nd = @match.records.where(half: 2).order(:recordtime)
    #@records1st = Record.where(match: @match, half: 1)
    #@records2nd = Record.where(match: @match, half: 2)
  end
    
  def new
    @match = Match.new
    @teams = Team.all
  end
    
 def edit
    @match = Match.find(params[:id])
    @teams = Team.all
 end
    
 def create
    permitted_params = params.require(:match).permit(:matchid, :date)
    @match = Match.create(permitted_params)
    @match.teams << Team.find(params[:team0])
    @match.teams << Team.find(params[:team1])
    @match.name = @match.teams.first.name + " vs. " + @match.teams.second.name
    if @match.save
#      redirect_to @match, notice: "New match registered."
        redirect_to :matches, notice: "New match registered."
    else
      @allteams = Team.all
      render "new"
    end
  end
    
  def update
    @match = Match.find(params[:id])
    #    @match.assign_attributes(params[:match])
    permitted_params = params.require(:match).permit(:matchid, :date)
    @match.update_attributes(permitted_params)
    @match.teams.delete_all
    @match.teams << Team.find(params[:team0])
    @match.teams << Team.find(params[:team1])
    @match.name = @match.teams.first.name + " vs. " + @match.teams.second.name
    if @match.save
      redirect_to @match, notice: "Match profile is updated."
#      redirect_to :matches, notice: "Match profile is updated."
    else
      @allteams = Team.all
      render "edit"
    end
  end
    
  def destroy
    @match = Match.find(params[:id])
    @match.destroy
    redirect_to :matches, notice: "Match deleted!"
  end
    
# 検索
  def search
    @matchs = Match.search(params[:q]).
    paginate(page: params[:page], per_page: 15)
    render "index"
  end
end
