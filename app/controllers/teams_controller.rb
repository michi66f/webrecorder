# cording: utf-8

class TeamsController < ApplicationController

  def index
    @teams = Team.order("name")
  end

  def show
    @team = Team.find(params[:id])
    if params[:format].in?(["jpg", "png", "gif"])
      send_image
      #    else
      #render "teams/show"
    end
  end

  def new
    @team = Team.new
  end

  def edit
    @team = Team.find(params[:id])
  end

  def create
    permitted_params = params.require(:team).permit(:name, :hometown, :stadium)
    @team = Team.create(permitted_params)
    if @team.save
#      redirect_to @team, notice: "New team registered."
      redirect_to :teams, notice: "New team registered."
    else
      render "new"
    end
  end

  def update
    @team = Team.find(params[:id])
    permitted_params = params.require(:team).permit(:name, :hometown, :stadium)
    @team.update_attributes(permitted_params)
    if @team.save
#        redirect_to @team, notice: "Team profile is updated."
        redirect_to :teams, notice: "Team profile is updated."
    else
      render "edit"
    end
  end

  def destroy
    @team = Team.find(params[:id])
    @team.destroy
    redirect_to :teams, notice: "Team deleted!"
  end

# 検索
  def search
    @teams = Team.search(params[:q]).
    paginate(page: params[:page], per_page: 15)
    render "index"
  end

  private
  # 画像送信
  def send_image
    if @team.image.present?
      send_data @team.image.data,
      type: @team.image.content_type, disposition: "inline"
    else
      raise NotFound
    end
  end
end
