class Match < ActiveRecord::Base
  has_many :records
  has_and_belongs_to_many :teams

  validates :matchid,
    presence: true,
    length: { minimum: 5, maximum: 20, allow_blank: true }

#  def name
#    if (self.teams.all.first && self.teams.all.second)
#      self.teams.first.name + " vs. " + self.teams.second.name
#    end
#  end
  
  class << self
  def search(query)
      rel = order("name")
      if query.present?
        rel = rel.where("name LIKE ?", "%#{query}%")
      end
      rel
    end
  end

end
