class Team < ActiveRecord::Base
  has_and_belongs_to_many :matches
  has_one :image, class_name: "TeamImage", dependent: :destroy

  validates :name,
    presence: true,
    length: { minimum: 1, maximum: 20, allow_blank: true }

  class << self
    def search(query)
      rel = order("name")
      if query.present?
        rel = rel.where("name LIKE ?", "%#{query}%")
      end
      rel
    end
  end

end
