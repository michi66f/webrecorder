class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
#      t.references :match, null: false  # 外部キー
      t.references :match               # 外部キー
      t.integer :team                   # home(0)/away(1)
      t.string :event                   # 記録の種類（パス、ドリブル、シュート、得点）
      t.integer :half                   # 前半(1)/後半(2)
      t.integer :recordtime             # 記録時刻（秒単位）
      t.timestamps
    end
  end
end
