class CreateTeamImages < ActiveRecord::Migration
  def change
    create_table :team_images do |t|
      t.references :team, null: false # 外部キー
      t.binary :data                  # 画像データ
      t.string :content_type          # MIMEタイプ
      t.timestamps
    end
    
    add_index :team_images, :team_id
  end
end
