class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.string :matchid     # 試合のID
      t.string :name        # 名称
      t.date :date          # 日付
      t.time :kickoff1st    # 前半のキックオフ時刻
      t.time :kickoff2nd    # 後半のキックオフ時刻
      t.timestamps
    end
  end
end
