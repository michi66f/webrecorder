class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name     # チーム名
      t.string :hometown # ホームタウン
      t.string :stadium  # スタジアム
      t.string :logofile # ロゴのファイル名
      t.timestamps
    end
  end
end
