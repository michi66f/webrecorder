# coding: utf-8

names = ["アルビレックス", "アントラーズ", "アルディージャ", "セレッソ", "FC東京", "フロンターレ", "ガンバ", "グランパス", "マリノス", "レッズ", "レイソル", "サガン", "サンフレッチェ", "エスパルス", "ベガルタ", "ヴァンフォーレ", "ヴィッセル", "ヴォルティス"]
hometowns = ["新潟", "鹿島", "大宮", "大阪", "東京", "川崎", "大阪", "名古屋", "横浜", "浦和", "柏", "鳥栖", "広島", "清水", "仙台", "甲府", "神戸", "徳島"]
stadiums = ["ビッグスワンスタジアム", "カシマスタジアム", "NACK5スタジアム", "ヤンマースタジアム", "味の素スタジアム", "等々力陸上競技場", "万博記念競技場", "豊田スタジアム", "日産スタジアム", "埼玉スタジアム2002", "日立柏サッカー場", "ベストアメニティスタジアム", "エディオンスタジアム", "IAIスタジアム日本平", "ユアテックスタジアム", "山梨中銀スタジアム", "ノエビアスタジアム", "ポカリスエットスタジアム"]
fnames = ["albirex", "antlers", "ardija", "cerezo", "fctokyo", "frontale", "gamba", "grampus", "marinos", "reds", "reysol", "sagan", "sanfrecce", "spuls", "vegalta", "ventforet", "vissel", "voltis"]

0.upto(17) do |idx|
  team = Team.create(
           name: names[idx],
           hometown: hometowns[idx],
           stadium: stadiums[idx]
         )

  fname = Rails.root.join("db/seeds/development/j-league", fnames[idx]+".png")
    TeamImage.create(
      team: team,
      data: File.open(fname, "rb").read,
      content_type: "image/png"
    )
end
