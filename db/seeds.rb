# coding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#team1 = Team.create(name: "ベガルタ", hometown: "仙台", stadium: "ユアテックスタジアム")
#team2 = Team.create(name: "アントラーズ", hometown: "鹿島", stadium: "カシマスタジアム")
#team3 = Team.create(name: "レイソル", hometown: "柏", stadium: "日立柏サッカー場")
#team4 = Team.create(name: "レッズ", hometown: "浦和", stadium: "埼玉スタジアム2002")
#team5 = Team.create(name: "アルディージャ", hometown: "大宮", stadium: "NACK5スタジアム")
#team6 = Team.create(name: "FC東京", hometown: "東京", stadium: "味の素スタジアム")
#team7 = Team.create(name: "フロンターレ", hometown: "川崎", stadium: "等々力陸上競技場")
#team8 = Team.create(name: "F・マリノス", hometown: "横浜", stadium: "日産スタジアム")
#team9 = Team.create(name: "ヴァンフォーレ", hometown: "甲府", stadium: "山梨中銀スタジアム")
#team10 = Team.create(name: "エスパルス", hometown: "清水", stadium: "IAIスタジアム日本平")
#team11 = Team.create(name: "グランパス", hometown: "名古屋", stadium: "豊田スタジアム")
#team12 = Team.create(name: "ガンバ", hometown: "大阪", stadium: "万博記念競技場")
#team13 = Team.create(name: "セレッソ", hometown: "大阪", stadium: "ヤンマースタジアム")
#team14 = Team.create(name: "ヴィッセル", hometown: "神戸", stadium: "ノエビアスタジアム")
#team15 = Team.create(name: "サンフレッチェ", hometown: "広島", stadium: "エディオンスタジアム")
#team16 = Team.create(name: "ヴォルティス", hometown: "徳島", stadium: "ポカリスエットスタジアム")
#team17 = Team.create(name: "サガン", hometown: "鳥栖", stadium: "ベストアメニティスタジアム")

# Teamの初期化
table_names = %w(teams)
table_names.each do |table_name|
  path = Rails.root.join("db/seeds", Rails.env, table_name + ".rb")
  if File.exist?(path)
    puts "Creating #{table_name}..."
    require path
  end
end

match = Match.create(matchid: "xxxxxxxx")
match.teams << Team.find(1)
match.teams << Team.find(2)
match.name = match.teams.first.name + " vs. " + match.teams.second.name
match.save

match = Match.create(matchid: "yyyyyyyy")
match.teams << Team.find(3)
match.teams << Team.find(4)
match.name = match.teams.first.name + " vs. " + match.teams.second.name
match.save

record = Record.create(team:0, event:"PASS", half:0, recordtime:6)
match.records << record
record = Record.create(team:1, event:"DRRIBLE", half:0, recordtime:8)
match.records << record
record = Record.create(team:1, event:"SHOT", half:0, recordtime:15)
match.records << record
record = Record.create(team:0, event:"GOAL", half:0, recordtime:15)
match.records << record
record = Record.create(team:0, event:"DRRIBLE", half:0, recordtime:20)
match.records << record
record = Record.create(team:1, event:"SHOT", half:0, recordtime:25)
match.records << record
record = Record.create(team:0, event:"GOAL", half:0, recordtime:25)
match.records << record
record = Record.create(team:0, event:"DRRIBLE", half:0, recordtime:30)
match.records << record
record = Record.create(team:1, event:"SHOT", half:0, recordtime:36)
match.records << record
record = Record.create(team:0, event:"PASS", half:0, recordtime:44)
match.records << record
record = Record.create(team:1, event:"PASS", half:0, recordtime:50)
match.records << record
record = Record.create(team:0, event:"PASS", half:0, recordtime:60)
match.records << record
record = Record.create(team:1, event:"PASS", half:0, recordtime:66)
match.records << record
record = Record.create(team:1, event:"DRRIBLE", half:0, recordtime:78)
match.records << record
record = Record.create(team:1, event:"PASS", half:0, recordtime:112)
match.records << record
match.save